#include "defines.hpp"
#include "app.hpp"

int main()
{
    DEBUG_PRINT("\nPROGRAM START\n");
    sf::VideoMode vm = sf::VideoMode::getDesktopMode();
    sf::RenderWindow window(
        sf::VideoMode(
            vm.width,
            vm.height,
            vm.bitsPerPixel
        ),
        WINDOW_TITLE,
        sf::Style::Fullscreen
    );
    window.setVerticalSyncEnabled(true);
    App app(&window);
    return app.onExecute();
}