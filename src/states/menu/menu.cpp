#include "menu.hpp"

/* Обработчики нажатий на кнопку */
int onStartGameClick(sf::Event event, sf::RenderWindow *w, ui::Label *target) {
    return STATE_GAME;
}

int onExitGameClick(sf::Event event, sf::RenderWindow *w, ui::Label *target) {
    return STATE_EXIT;
}

int onCredentialsClick(sf::Event event, sf::RenderWindow *w, ui::Label *target) {
    return STATE_CREDENTIALS;
}

MenuState::MenuState(sf::RenderWindow *w, sf::Font *font) {
    const int labelAxisY = w->getSize().x / 2 - 200;
    labels.push_back(ui::Label("START GAME", font,labelAxisY, 100, 22, onStartGameClick));
    labels.push_back(ui::Label("CREDENTIALS", font, labelAxisY, 200, 22, onCredentialsClick));
    labels.push_back(ui::Label("EXIT", font, labelAxisY, 300, 22, onExitGameClick));
}

int MenuState::onEvent(sf::Event event, sf::RenderWindow *w) {
    if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
        DEBUG_PRINT("ESCAPE");
        return STATE_GAME;
    }
    int btnState = STATE_EMPTY;
    for (std::vector<ui::Label>::iterator it = labels.begin(); it != labels.end(); ++it) {
        if ((btnState = it->onClick(event, w)) != STATE_EMPTY) {
            DEBUG_PRINT("RETURN STATE %d", btnState);
            return btnState;
        }
    }
    return STATE_EMPTY;
}

void MenuState::onLoop(sf::RenderWindow *w) {
    for (std::vector<ui::Label>::iterator it = labels.begin(); it != labels.end(); ++it) {
        it->onLoop(w);
    }
}

void MenuState::onRender(sf::RenderWindow *w) {
    for (std::vector<ui::Label>::iterator it = labels.begin(); it != labels.end(); ++it) {
        it->onRender(w);
    }
}

MenuState::~MenuState() {}

void MenuState::onInput(void) {}
