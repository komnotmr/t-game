#pragma once

#include <vector>

#include "../state.hpp"
#include "../../ui/label/label.hpp"

int onStartGameClick(sf::Event event, sf::RenderWindow *w, ui::Label *target);
int onExitGameClick(sf::Event event, sf::RenderWindow *w, ui::Label *target);
int onCredentialsClick(sf::Event event, sf::RenderWindow *w, ui::Label *target);

class MenuState : public State {
    private:
        std::vector<ui::Label> labels;
    public:
        int onEvent(sf::Event, sf::RenderWindow *);
        void onLoop(sf::RenderWindow *);
        void onRender(sf::RenderWindow *);
        void onInput(void);
        ~MenuState();
        MenuState(sf::RenderWindow *, sf::Font*);
};
