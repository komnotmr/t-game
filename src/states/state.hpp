#pragma once
#include <SFML/Graphics.hpp>

#include <string>

#include "../defines.hpp"
#include "../ui/label/label.hpp"
#include "../gameObjects/player/player.hpp"
#include "../gameObjects/player/control.hpp"

class State {
    public:
        virtual int onEvent(sf::Event, sf::RenderWindow *) = 0;
        virtual void onLoop(sf::RenderWindow *) = 0;
        virtual void onRender(sf::RenderWindow *) = 0;
        virtual void onInput(void) = 0;
        virtual ~State() {};
};
