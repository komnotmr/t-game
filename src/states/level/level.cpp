#include "level.hpp"

Level::Level() {}

bool Level::loadFromFile(const char *filename) {
    std::ifstream in;
    in.open(filename);
    if (!in.is_open())
        return false;

    int playerPosX, playerPosY;
    for (int i = 0; i < 2; i++) {
        if (!(in >> playerPosX >> playerPosY)) {
            in.close();
            DEBUG_PRINT("Error on load level, not found player %d position", i);
            return false;
        }
        playersPos[i] = sf::Vector2f(playerPosX, playerPosY);

    }

    int objType, x, y, w, h;
    while (in >> objType >> x >> y >> w >> h) {
        switch (objType) {
            case  objTypes::block: {
                blocks.push_back(
                    Block(
                        sf::Vector2f(x, y),
                        sf::Vector2f(w, h),
                        sf::Color::Green
                    )
                );
            } break;
        
            default: {
                DEBUG_PRINT("unknown objType");
            } break;
        }
    }

    in.close();

    return true;
}
