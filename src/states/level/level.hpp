#pragma once

#include "../../gameObjects/block/block.hpp"

#include <iostream>
#include <fstream>

enum objTypes {
    none = 0,
    block
};

class Level {
    private:
        std::array<sf::Vector2f, 2> playersPos;
    public:
        std::vector<Block> blocks;
        Level();
        // void setPlayerPos(unsigned int playerInd, sf::IntRect playerPos); // ?
        // void pushGameObject(GameObject go); //?
        bool loadFromFile(const char *filename);
};

// level = Level();
// level.loadFromFile("levels/level0.txt");

// level.setPlayerPos(0, sf::IntRect(100, 100));
// level.setPlayerPos(1, sf::IntRect(100, 100));

// level.pushGameObject();
// level.pushGameObject();
// level.pushGameObject();
// level.pushGameObject();
// ...
// level.pushGameObject();

