#include "game.hpp"

/* Обработчики нажатий на кнопку */
int oncBackToMenuClick(sf::Event event, sf::RenderWindow *w, ui::Label *target) {
    return STATE_MENU;
}

GameState::GameState(sf::RenderWindow *w, sf::Font *font, sf::Texture *playerTexture) {
    DEBUG_PRINT("CREATE GAMESTATE");
    const int labelAxisY = w->getSize().x - 100;
    labels.push_back(ui::Label("Back to Menu", font, labelAxisY, 32, 12, oncBackToMenuClick));

    playersBulletCount.init("", font, 32, 96, 12, nullptr);

    players[0] = Player(100, 100, sf::Color::Green, playerTexture);
    players[1] = Player(200, 200, sf::Color::Yellow, playerTexture);

    controls.push_back(Control(
        sf::Keyboard::Key::Left,
        sf::Keyboard::Key::Right,
        sf::Keyboard::Key::Up,
        sf::Keyboard::Key::Down,
        sf::Keyboard::Key::RControl
    ));

    controls.push_back(Control(
        sf::Keyboard::Key::A,
        sf::Keyboard::Key::D,
        sf::Keyboard::Key::W,
        sf::Keyboard::Key::S,
        sf::Keyboard::Key::E
    ));

    players[0].setControls(&controls[0]);
    players[1].setControls(&controls[1]);

    /* init levels */
    levels.push_back(Level());
    levels.back().loadFromFile("levels/level0.txt");
}

int GameState::onEvent(sf::Event event, sf::RenderWindow *w) {
    /*debug end */
    if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
        DEBUG_PRINT("ESCAPE");
        return STATE_MENU;
    }
    int btnState = STATE_EMPTY;
    /* labels */
    for (std::vector<ui::Label>::iterator it = labels.begin(); it != labels.end(); ++it) {
        if ((btnState = it->onClick(event, w)) != STATE_EMPTY) {
            DEBUG_PRINT("RETURN STATE %d", btnState);
            return btnState;
        }
    }
    /* players */
    for (int i = 0; i < 2; i++)
        players[i].onEvent(event, w);//, it);

    return STATE_EMPTY;
}

void GameState::onLoop(sf::RenderWindow *w) {
    for (std::vector<ui::Label>::iterator it = labels.begin(); it != labels.end(); ++it) {
        it->onLoop(w);
    }
    playersBulletCount.setText(
        sf::String(
            "player1 bullets: " +
            std::to_string(players[0].bullets.size()) +
            ", player2 bullets: " +
            std::to_string(players[1].bullets.size())
        )
    );
    /* players */
    for (int i = 0; i < 2; i++)
        players[i].onLoop(w);
    /* level objects */
    for (
        std::vector<Block>::iterator it = levels[currentLevel].blocks.begin();
        it != levels[currentLevel].blocks.end();
        it++
        ) {
            it->onLoop(w);
        }
}

void GameState::onRender(sf::RenderWindow *w) {
    for (std::vector<ui::Label>::iterator it = labels.begin(); it != labels.end(); ++it) {
        it->onRender(w);
    }
    playersBulletCount.onRender(w);
    /* players */
    for (int i = 0; i < 2; i++)
        players[i].onRender(w);
    /* level objects */
    for (
        std::vector<Block>::iterator it = levels[currentLevel].blocks.begin();
        it != levels[currentLevel].blocks.end();
        it++
        ) {
            it->onRender(w);
        }
}

void GameState::onInput(void) {
    for (int i = 0; i < 2; i++)
        players[i].onInput();
}

GameState::~GameState() {}
