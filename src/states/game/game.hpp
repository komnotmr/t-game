#pragma once

#include <vector>

#include "../state.hpp"
#include "../level/level.hpp"

int oncBackToMenuClick(sf::Event event, sf::RenderWindow *w, ui::Label *);

class GameState : public State {
    private:
        std::vector<ui::Label> labels;
        ui::Label playersBulletCount;
        std::array<Player, 2> players;
        std::vector<Control> controls;
        std::vector<Level> levels;
        unsigned int currentLevel = 0;
    public:
        int onEvent(sf::Event, sf::RenderWindow *);
        void onLoop(sf::RenderWindow *);
        void onRender(sf::RenderWindow *);
        void onInput(void);
        ~GameState();
        GameState(sf::RenderWindow *w, sf::Font*, sf::Texture*);
};
