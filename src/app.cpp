#include "app.hpp"

App::App(sf::RenderWindow *w) {
    running = true;
    window = w;
}

bool App::onInit() {
    if (!font.loadFromFile("assets/fonts/RobotoMono-VariableFont_wght.ttf")) {
        DEBUG_PRINT("ERROR ON LOAD RobotoMono-VariableFont_wght.ttf");
        return false;
    }

    fpsLabel.init("TICKS 0", &font, 32, 32, 12, nullptr);
    mouseCoords.init("(0, 0)", &font, 32, 64, 12, nullptr);

    if (!playerImage.loadFromFile("assets/img/hero.png"))   {
        DEBUG_PRINT("ERROR ON LOAD hero.png");
        return false;
    }

    // playerImage.createMaskFromColor(sf::Color(255, 255, 255));

    playerTexture.loadFromImage(playerImage);

    MenuState *menu = new MenuState(window, &font);
    GameState *game = new GameState(window, &font, &playerTexture);
    states.insert(std::pair<int, State*>(STATE_MENU, menu));
    states.insert(std::pair<int, State*>(STATE_GAME, game));
    currState = states[STATE_MENU];
    return true;
}

void App::drawMouseCoordinates () {
    sf::Vector2i lastMousePos = sf::Mouse::getPosition(*window);
    mouseCoords.setText(sf::String("(" + std::to_string(lastMousePos.x) + ", " + std::to_string(lastMousePos.y) + ")"));
}

int App::onExecute() {
    if (!onInit())
        return -1;
 
    sf::Event event;
    int nextState = STATE_EMPTY;

    sf::Clock clock;
    sf::Time prevTime = clock.getElapsedTime();
    int frameCounter = 0;
    while (running) {
        currState->onInput();
        while (window->pollEvent(event)) {
            drawMouseCoordinates();
            if (event.type == sf::Event::Closed) {
                window->close();
                running = false;
            }
            if ((nextState = currState->onEvent(event, window)) != STATE_EMPTY) {
                if (nextState == STATE_EXIT) {
                    running = false;
                    window->close();
                } else {
                    /* TODO currState.pause()*/
                    currState = states[nextState];
                    /* TODO currState.resume()*/
                }
            }
        }

        if (nextState == STATE_EXIT)
            break;            
 
        sf::Time currentTime = clock.getElapsedTime();
        if (currentTime.asMilliseconds() - prevTime.asMilliseconds() >= MS_PER_TICK) {
            prevTime = currentTime;
            frameCounter = 0;
            /* loop step start */
            currState->onLoop(window);
            /* loop step end */
            /* render step start */
            window->clear();
            currState->onRender(window);
            mouseCoords.onRender(window);
            /*render step end*/
        }
        frameCounter++;
        fpsLabel.setText(sf::String("TICKS " + std::to_string(frameCounter)));
        fpsLabel.onRender(window);
        window->display();
    }
 
    onCleanup();
 
    return 0;
}

void App::onCleanup() {
    std::map<int, State*>::iterator it;
    for (it = states.begin(); it != states.end(); it++) { delete it->second; }
};
