#pragma once
#include <SFML/Graphics.hpp>

#include <map>

#include "ui/label/label.hpp"

#include "states/menu/menu.hpp"
#include "states/game/game.hpp"

#include "gameObjects/player/player.hpp"

class App {
    private:
        bool running;
        sf::RenderWindow *window;
        sf::Font font;
        sf::Texture playerTexture;
        sf::Image playerImage;
        ui::Label fpsLabel;
        ui::Label mouseCoords;
        std::map<int, State*> states;
        State *currState;
        void drawMouseCoordinates();
        float prevFps;
    public:
        App(sf::RenderWindow *);
        int onExecute();

        bool onInit();
        void onCleanup();
};
