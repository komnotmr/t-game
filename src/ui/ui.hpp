#pragma once

#include <SFML/Graphics.hpp>
#include "../defines.hpp"

namespace ui {
    class BaseUI {
        public:
            virtual void onRender(sf::RenderWindow *) = 0;
            virtual void onLoop(sf::RenderWindow *) = 0;
            virtual ~BaseUI() {};
    };
}