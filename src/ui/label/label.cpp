 #include "label.hpp"
 
 namespace ui {
    Label::Label() {}

    Label::Label(
        const char *t,
        sf::Font *font,
        float x,
        float y,
        float fontSize,
        int (*__onEvent)(sf::Event, sf::RenderWindow *, Label *)
    ) {
        init(t, font, x, y, fontSize, __onEvent);
    }

    void Label::init(
        const char *t,
        sf::Font *font,
        float x,
        float y,
        float fontSize,
        int (*__onEvent)(sf::Event, sf::RenderWindow *, Label *)
    ) {
        text.setString(sf::String(t));
        text.setFont(*font);
        text.setPosition(x, y);
        text.setCharacterSize(fontSize);
        text.setFillColor(sf::Color::White);
        initFontSize = fontSize;
        this->__onEvent = __onEvent;
    }

    void Label::onLoop(sf::RenderWindow *w) {
        sf::Vector2i lastMousePos = sf::Mouse::getPosition(*w);
        sf::FloatRect globalBoundText = text.getGlobalBounds();
        if (globalBoundText.contains(lastMousePos.x, lastMousePos.y)) {
            text.setCharacterSize(initFontSize * 1.1);
            text.setFillColor(sf::Color::Yellow);
        } else {
            text.setCharacterSize(initFontSize);
            text.setFillColor(sf::Color::White);
        }
    }

    int Label::onClick(sf::Event e, sf::RenderWindow *w) {
        if (e.type == sf::Event::MouseButtonPressed && e.mouseButton.button == sf::Mouse::Left) {
            sf::Vector2i lastMousePos = sf::Mouse::getPosition(*w);
            sf::FloatRect globalBoundText = text.getGlobalBounds();
            if (globalBoundText.contains(lastMousePos.x, lastMousePos.y))
                return __onEvent ? __onEvent(e, w, this) : STATE_EMPTY;
        }
        return STATE_EMPTY;
    }

    void Label::onRender(sf::RenderWindow *w) {
        w->draw(text);
    }

    void Label::setText(sf::String str) {
        text.setString(str);
    }

}
 