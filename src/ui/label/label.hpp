#pragma once

#include "../ui.hpp"

namespace ui {
    class Label : public BaseUI {
        private:
            sf::Text text;
            sf::Vector2i lastMousePos;
            int initFontSize;
            int (*__onEvent)(sf::Event, sf::RenderWindow *, Label *);
        public:
            int onClick(sf::Event, sf::RenderWindow *);
            Label ();
            Label(
                const char *t,
                sf::Font *font,
                float x,
                float y,
                float fontSize,
                int (*__onEvent)(sf::Event, sf::RenderWindow *, Label *)
            );
            void init (
                const char *t,
                sf::Font *font,
                float x,
                float y,
                float fontSize,
                int (*__onEvent)(sf::Event, sf::RenderWindow *, Label *)
            );
            void setText(sf::String str);
            void onLoop(sf::RenderWindow *);
            void onRender(sf::RenderWindow *);
    };
}