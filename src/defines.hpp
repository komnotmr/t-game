#pragma once

#define WINDOW_TITLE "GAME TITLE"

#define DEBUG 1

#ifdef DEBUG
    #include <stdio.h>
    #define DEBUG_PRINT(fmt, ...) \
            {FILE *f = fopen("debug.file", "a"); \
            if (f) {\
            fprintf(f, fmt, ##__VA_ARGS__); \
            fprintf(f, "\n"); \
            fclose(f);}}
#else
    #define DEBUG_PRINT(fmt, ...)
#endif

#define STATE_GAME 1
#define STATE_MENU 2
#define STATE_CREDENTIALS 3
#define STATE_EXIT 4
#define STATE_EMPTY 0

#define MS_IN_SEC 1000
#define SCREEN_FPS 60
#define MS_PER_TICK (MS_IN_SEC / SCREEN_FPS)
