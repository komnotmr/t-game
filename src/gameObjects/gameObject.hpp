#pragma once

#include <SFML/Graphics.hpp>

#include "../defines.hpp"

class GameObject {
    public:
        virtual ~GameObject() {}
};
