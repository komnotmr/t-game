#pragma once

#include "../gameObject.hpp"

class Block : public GameObject {
    public:
        sf::RectangleShape rect;
        int onEvent(sf::Event, sf::RenderWindow *);
        Block ();
        Block(
            sf::Vector2f pos,
            sf::Vector2f size,
            sf::Color c
        );
        void init (
            sf::Vector2f pos,
            sf::Vector2f size,
            sf::Color c
        );
        void setMoveVector(float, float);
        void onLoop(sf::RenderWindow *);
        void onRender(sf::RenderWindow *);
};
