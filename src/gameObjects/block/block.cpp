#include "block.hpp"

Block::Block() {};

Block::Block(
            sf::Vector2f pos,
            sf::Vector2f size,
            sf::Color c
) {
    this->init(pos, size, c);
}

void Block::init(
            sf::Vector2f pos,
            sf::Vector2f size,
            sf::Color c
) {
    rect.setPosition(pos);
    rect.setSize(size);
    rect.setFillColor(c);
}

int Block::onEvent(sf::Event, sf::RenderWindow *) {
    return STATE_EMPTY;
}

void Block::onLoop(sf::RenderWindow *w) {}

void Block::onRender(sf::RenderWindow *w) {
    w->draw(rect);
}
