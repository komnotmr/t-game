#pragma once

#include "../../defines.hpp"
#include <SFML/Graphics.hpp>

class Control {
    private:
        sf::Keyboard::Key Left;
        sf::Keyboard::Key Right;
        sf::Keyboard::Key Up;
        sf::Keyboard::Key Down;
        sf::Keyboard::Key Fire;

    public:
        Control();
        Control(
            sf::Keyboard::Key l,
            sf::Keyboard::Key r,
            sf::Keyboard::Key u,
            sf::Keyboard::Key d,
            sf::Keyboard::Key f
        );

        sf::Keyboard::Key getLeft();
        sf::Keyboard::Key getRight();
        sf::Keyboard::Key getUp();
        sf::Keyboard::Key getDown();
        sf::Keyboard::Key getFire();

        bool setLeft(sf::Keyboard::Key);
        bool setRight(sf::Keyboard::Key);
        bool setUp(sf::Keyboard::Key);
        bool setDown(sf::Keyboard::Key);
        bool setFire(sf::Keyboard::Key);
};
