#pragma once

#include "../gameObject.hpp"
#include "../animation/animation.hpp"
#include "../bullet/bullet.hpp"

#include "control.hpp"

enum AnimationStates {
    idle = 0,
    left,
    right,
    up,
    down
};

class Player : public GameObject {
    private:
        sf::CircleShape shape;
        Control *control;
        float x;
        float y;
        float ax;
        float ay;
        float dirx;
        float diry;
        float _ax = 3;
        float _ay = 3;
        bool fireAlreadyPressed = false;
        AnimationStates state;
        std::map<AnimationStates, Animation> animation;
        sf::Sprite sprite;
        void animationInit();
    public:
        std::vector<Bullet> bullets;
        int onEvent(sf::Event, sf::RenderWindow *);
        Player ();
        Player(
            float x,
            float y,
            sf::Color c,
            sf::Texture *t
        );
        bool setControls(Control *c);
        void init (
            float x,
            float y,
            sf::Color c,
            sf::Texture *t
        );
        void onLoop(sf::RenderWindow *);
        void onRender(sf::RenderWindow *);
        void onInput(void);
};
