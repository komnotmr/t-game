#include "control.hpp"

Control::Control() {}

Control::Control(
    sf::Keyboard::Key l,
    sf::Keyboard::Key r,
    sf::Keyboard::Key u,
    sf::Keyboard::Key d,
    sf::Keyboard::Key f
) {
    Left = l;
    Right = r;
    Up = u;
    Down = d;
    Fire = f;
}

sf::Keyboard::Key Control::getLeft() {
    return Left;
}
sf::Keyboard::Key Control::getRight() {
    return Right;
}
sf::Keyboard::Key Control::getUp() {
    return Up;
}
sf::Keyboard::Key Control::getDown() {
    return Down;
}
sf::Keyboard::Key Control::getFire() {
    return Fire;
}

bool Control::setLeft(sf::Keyboard::Key k ) {Left = k; return true;}
bool Control::setRight(sf::Keyboard::Key k) {Right = k; return true;}
bool Control::setUp(sf::Keyboard::Key k) {Up = k; return true;}
bool Control::setDown(sf::Keyboard::Key k) {Down = k; return true;}
bool Control::setFire(sf::Keyboard::Key k) {Fire = k; return true;}
