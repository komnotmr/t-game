 #include "player.hpp"
 
Player::Player() {}

Player::Player(
    float x,
    float y,
    sf::Color c,
    sf::Texture *t
) {
    init(x, y, c, t);
}

void Player::animationInit() {
    /* init animations*/
    Animation *a = nullptr;
    const int width = 96;
    const int height = 96;
    const int durationFrame = 4; // one frame per 8 ticks

    animation.insert({AnimationStates::idle, Animation()});
    a = &animation[AnimationStates::idle];
    a->pushFrame(0 * width, 0 * height, width, height, durationFrame);
    a->pushFrame(1 * width, 0 * height, width, height, durationFrame);
    a->pushFrame(2 * width, 0 * height, width, height, durationFrame);
    a->pushFrame(1 * width, 0 * height, width, height, durationFrame);
    a->pushFrame(0 * width, 0 * height, width, height, durationFrame);

    animation.insert({AnimationStates::left, Animation()}); // state left
    a = &animation[AnimationStates::left];
    a->pushFrame(0 * width, 1 * height, width, height, durationFrame);
    a->pushFrame(1 * width, 1 * height, width, height, durationFrame);
    a->pushFrame(2 * width, 1 * height, width, height, durationFrame);

    animation.insert({AnimationStates::right, Animation()}); // state right
    a = &animation[AnimationStates::right];
    a->pushFrame(0 * width, 2 * height, width, height, durationFrame);
    a->pushFrame(1 * width, 2 * height, width, height, durationFrame);
    a->pushFrame(2 * width, 2 * height, width, height, durationFrame);
}

void Player::init(
    float x,
    float y,
    sf::Color c,
    sf::Texture *t
) {
    shape.setFillColor(c);
    shape.setRadius(50.f);
    this->x = x;
    this->y = y;
    shape.setPosition(x, y);
    ay = 30;
    ax = 30;
    sprite.setTexture(*t);
    sprite.setColor(c);
    animationInit();
    state = AnimationStates::idle;
    dirx = 0;
    diry = 0;
}

bool Player::setControls(Control *c) {
    control = c;
    return true;
}

void Player::onLoop(sf::RenderWindow *w) {
    if (dirx != 0)
        x = ax*dirx + x;
    if (diry != 0)
        y = ay*diry + y;
    if (ax > 0) ax = ax - 0.1 < 0 ? 0 : ax - 0.1;
    if (ay > 0) ay = ay - 0.1 < 0 ? 0 : ay - 0.1;
    shape.setPosition(x, y);
    sprite.setPosition(x, y);

    for (std::vector<Bullet>::iterator it = bullets.begin(); it != bullets.end();) {
        it->onLoop(w);
        sf::Vector2f bPos = it->shape.getPosition();
        sf::Vector2u wSize = w->getSize();
        if (bPos.x < 0 || bPos.y < 0 || bPos.x >= wSize.x || bPos.y >= wSize.y) {
            bullets.erase(it);
        } else it++;
    }
}

int Player::onEvent(sf::Event e, sf::RenderWindow *w) { return 0; }

void Player::onInput(void) {
    state = AnimationStates::idle;

    if (sf::Keyboard::isKeyPressed(control->getLeft())) {
        DEBUG_PRINT("PLAYER KEY LEFT");
        dirx = -1;
        ax = _ax;
        state = AnimationStates::left;
    } else if (sf::Keyboard::isKeyPressed(control->getRight())) {
        DEBUG_PRINT("PLAYER KEY RIGHT");
        dirx = 1;
        ax = _ax;
        state = AnimationStates::right;
    }

    if (sf::Keyboard::isKeyPressed(control->getUp())) {
        DEBUG_PRINT("PLAYER KEY UP");
        diry = -1;
        ay = _ay;
    } else if (sf::Keyboard::isKeyPressed(control->getDown())) {
        DEBUG_PRINT("PLAYER KEY DOWN");
        diry = 1;
        ay = _ay;
    }

    if (sf::Keyboard::isKeyPressed(control->getFire()) && !fireAlreadyPressed) {
        DEBUG_PRINT("PLAYER KEY FIRE");
        fireAlreadyPressed = true;
        bullets.push_back(Bullet(x, y, sf::Color::Red));
        bullets.back().setMoveVector(dirx * 10, 0);
    }

    if (fireAlreadyPressed && !sf::Keyboard::isKeyPressed(control->getFire())) {
        fireAlreadyPressed = false;
    }
}

void Player::onRender(sf::RenderWindow *w) {
    w->draw(shape); // for debug if needed
    sprite.setTextureRect(animation[state].animate());
    w->draw(sprite);
    for (std::vector<Bullet>::iterator it = bullets.begin(); it != bullets.end(); it++)
        w->draw(it->shape);
}
