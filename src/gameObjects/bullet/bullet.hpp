#pragma once

#include "../gameObject.hpp"

class Bullet : public GameObject {
    private:
        float dx;
        float dy;
    public:
        sf::CircleShape shape;
        int onEvent(sf::Event, sf::RenderWindow *);
        Bullet ();
        Bullet(
            float,
            float,
            sf::Color
        );
        void init (
            float,
            float,
            sf::Color c
        );
        void setMoveVector(float, float);
        void onLoop(sf::RenderWindow *);
        void onRender(sf::RenderWindow *);
};
