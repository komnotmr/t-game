#include "bullet.hpp"

Bullet::Bullet() {};

Bullet::Bullet(
    float x,
    float y,
    sf::Color c
) {
    this->init(x, y, c);
}

void Bullet::init(
    float x,
    float y,
    sf::Color c
) {
    shape.setRadius(4.f);
    shape.setFillColor(c);
    shape.setPosition(x, y);
}

void Bullet::setMoveVector(float dx, float dy) {
    this->dx = dx;
    this->dy = dy;
}

int Bullet::onEvent(sf::Event, sf::RenderWindow *) {
    return STATE_EMPTY;
}

void Bullet::onLoop(sf::RenderWindow *w) {
    sf::Vector2f pos = shape.getPosition();
    shape.setPosition(pos.x + dx, pos.y + dy);
}

void Bullet::onRender(sf::RenderWindow *w) {
    w->draw(shape);
}
