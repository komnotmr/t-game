#pragma once

#include <SFML/Graphics.hpp>

#include "../../defines.hpp"

#include <vector>

class Aframe {
    public:
        unsigned int frameCounter;
        sf::IntRect frameRange;
        unsigned int ticksPerFrame;

    Aframe(
        int x,
        int y,
        int w,
        int h,
        unsigned int tpf
    );
};

class Animation {
    private:
        std::vector<Aframe> frames;
        int currentFrame = 0;
        int framesCount = 0;
    public:
        Animation();
        sf::IntRect animate();
        bool pushFrame(
            int x,
            int y,
            int w,
            int h,
            unsigned int tpf
        );
};
