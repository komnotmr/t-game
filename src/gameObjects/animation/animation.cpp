#include "animation.hpp"

Aframe::Aframe(
    int x,
    int y,
    int w,
    int h,
    unsigned int tpf
) {
    frameRange = sf::IntRect(x, y, w, h);
    frameCounter = 0;
    ticksPerFrame = tpf;
}

Animation::Animation() {}

bool Animation::pushFrame(
    int x,
    int y,
    int w,
    int h,
    unsigned int tpf
) {
    frames.push_back(Aframe(x, y, w, h, tpf));
    framesCount++;
    return true;
}

sf::IntRect Animation::animate() {
    Aframe *currFrame = &frames[currentFrame];
    if (++currFrame->frameCounter >= currFrame->ticksPerFrame) {
        currFrame->frameCounter = 0;
        if (currentFrame >= framesCount) currentFrame = 0;
        return frames[currentFrame++].frameRange;
    }
    return currFrame->frameRange;
}
