# README #

Проект t-game.

### Сборка под Linux###

* Сборка `./common_build.sh build` 

### Сборка под Windows из под git bash ###

* Нужно скачать компилятор [MinGW 7.3.0 (x64)](https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win64/Personal%20Builds/mingw-builds/7.3.0/threads-posix/seh/x86_64-7.3.0-release-posix-seh-rt_v5-rev0.7z/download)
* Распаковать в каталог lib/windows
* Сборка `./common_build.sh build` 

### Правила стиля кода ###
* Пока нет

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact