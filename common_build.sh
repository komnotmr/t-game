#!/usr/bin/env bash
root=`pwd`

if [[ "$OSTYPE" == "msys" ]]; then #windows
    compiler=$root/lib/windows/mingw64
    export GXX=$compiler/bin/g++.exe
    export LD=$compiler/bin/ld.exe
    export GMAKE=$compiler/bin/mingw32-make.exe
    system=windows
else # linux
    export GXX=g++
    export LD=ld
    export GMAKE=make
    system=linux
fi

export SOURCE_DIR=$root/src
export EXECUTABLE_NAME=game.bin
export BIN_DIR=$root/bin
export SFML_DIR=$root/lib/$system/SFML-2.5.1
export SFML_LIBS='-lsfml-graphics -lsfml-window -lsfml-system'

help () {
    echo 'Usage: <build | run | clean | fullclean | cleanrun'
    echo 'build - build binary'
    echo 'build - run buided binary'
    echo 'clean - clean object files'
    echo 'cleanrun - eq calls with fullclean, build && run'
    echo 'fullclean - clean object files and binary'
}

if [ -z "$1" -o "$1" == "help" ]; then
    help
    exit 0
fi

if [ "$1" == "cleanrun" ]; then
    $GMAKE fullclean
    $GMAKE build
fi

if [ "$1" == "run" -o "$1" == "cleanrun" ]; then
    if [ "$system" == "linux" ]; then
        export LD_LIBRARY_PATH="$SFML_DIR/lib" && cd $BIN_DIR && $BIN_DIR/$EXECUTABLE_NAME
        exit 0
    else
        cd $BIN_DIR && $BIN_DIR/$EXECUTABLE_NAME
        exit 0
    fi
fi

echo $GMAKE
echo $GXX
echo $SOURCE_DIR
echo $SFML_LIBS
echo $SFML_DIR
echo "start make"
$GMAKE $1
