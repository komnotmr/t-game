.PHONY: clean fullclean

build:
	$(GMAKE) -C src main

clean:
	$(GMAKE) -C src $@

fullclean:
	$(GMAKE) -C src $@
